import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
	strict: true,
	state: {
		//Array con los artículos (voy a crear un resumen rápido de los articulos aquí mismo como si lo generase el backend)
		articles: [],
		article: [],
	},
	getters: {
		getArticle: (state) => {
			return state.article;
		},
		getArticles: (state) => {
			return state.articles;
		},
	},
	mutations: {
		setArticle: (state, article) => {
			state.article = article;
		},
		setArticles: (state, articles) => {
			state.articles = articles;
		},
	},
	actions: {
		fetchArticle: async ({ commit }, id) => {
			const articulo = await simularRetraso(simularGetArticleById(id));

			commit("setArticle", articulo);
		},
		fetchArticles: async ({ commit }) => {
			const articulos = await simularRetraso(simularGetArticles());
			commit("setArticles", articulos);
		},
	},
	modules: {},
});

//Simulo unas peticiones al back con retraso de unso milisegundos:
function simularGetArticles() {
	return [
		{
			title: "Lorem Ipsum",
			summary:
				"Lorem ipsum dolor sit amet, consectetur adipiscing elit...",
			id: 1,
		},
		{
			title: "Nullam mattis",
			summary: "Nullam mattis ullamcorper tincidunt. Morbi et...",
			id: 2,
		},
		{
			title: "Suspendisse finibus",
			summary:
				"Suspendisse finibus, nunc ut egestas malesuada, justo lacus...",
			id: 3,
		},
	];
}

function simularGetArticleById(id) {
	const articles = [
		{
			title: "Lorem Ipsum",
			text: `<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at vulputate odio. Mauris eu pulvinar tortor. Etiam pharetra tempor dui eget ultrices. Mauris pulvinar, lacus ac consectetur consequat, lacus diam bibendum dui, id sagittis tellus diam at enim. Vestibulum aliquam tempus arcu sed iaculis. Praesent fermentum condimentum tellus, quis lobortis metus imperdiet eu.</p> 
			<br>

			<img src="https://media.giphy.com/media/Vuw9m5wXviFIQ/giphy.gif">
			<br><br>
			
			<p>We're no strangers to love
You know the rules and so do I
A full commitment's what I'm thinking of
You wouldn't get this from any other guy
I just wanna tell you how I'm feeling
Gotta make you understand
Never gonna give you up
Never gonna let you down
Never gonna run around and desert you
Never gonna make you cry
Never gonna say goodbye
Never gonna tell a lie and hurt you
We've known each other for so long
Your heart's been aching but you're too shy to say it
Inside we both know what's been going on
We know the game and we're gonna play it
And if you ask me how I'm feeling
Don't tell me you're too blind to see
Never gonna give you up
Never gonna let you down
Never gonna run around and desert you
Never gonna make you cry
Never gonna say goodbye
Never gonna tell a lie and hurt you
Never gonna give you up
Never gonna let you down
Never gonna run around and desert you
Never gonna make you cry
Never gonna say goodbye
Never gonna tell a lie and hurt you
Never gonna give, never gonna give
(Give you up)
(Ooh) Never gonna give, never gonna give
(Give you up)
We've known each other for so long
Your heart's been aching but you're too shy to say it
Inside we both know what's been going on
We know the game and we're gonna play it
I just wanna tell you how I'm feeling
Gotta make you understand
Never gonna give you up
Never gonna let you down
Never gonna run around and desert you
Never gonna make you cry
Never gonna say goodbye
Never gonna tell a lie and hurt you
Never gonna give you up
Never gonna let you down
Never gonna run around and desert you
Never gonna make you cry
Never gonna say goodbye
Never gonna tell a lie and hurt you
Never gonna give you up
Never gonna let you down
Never gonna run around and desert you
Never gonna make you cry</p>`,
		},
		{
			title: "Nullam mattis",
			text: `<p>Nullam mattis ullamcorper tincidunt. Morbi et neque et diam fringilla mattis non et ipsum. Nulla mattis lobortis magna, eget vulputate est vehicula sed. Aliquam fermentum porta ipsum eu viverra. Fusce ullamcorper, elit id tincidunt dignissim, velit ex suscipit mauris, at vestibulum neque nulla id tellus. Etiam non tristique sapien. Mauris eu tellus neque.</p>
				<br><br>
				<h1>HOLI CHICOOOS, perdéis</h1>`,
		},
		{
			title: "Suspendisse finibus",
			text:
				"<p>Suspendisse finibus, nunc ut egestas malesuada, justo lacus ullamcorper arcu, a vehicula ante mi vitae metus. Mauris sed arcu tristique, molestie ante ut, rutrum mi. Proin faucibus tristique faucibus. In hac habitasse platea dictumst. Integer leo erat, malesuada non sem ut, aliquam ullamcorper eros. Vestibulum posuere fermentum laoreet. Maecenas at lorem a nibh sodales congue id eu erat. Vestibulum ornare, dolor a posuere gravida, mauris augue molestie risus, eu facilisis eros risus ac diam. Vivamus suscipit venenatis eros, vel convallis nibh gravida eu. Nulla varius pulvinar orci vitae convallis. Vestibulum molestie, odio in iaculis iaculis, lectus nulla tempus leo, in dignissim elit justo eget dolor. Vestibulum porttitor tempor diam, iaculis viverra massa tempor sit amet. Morbi tempor nisl venenatis vulputate tincidunt.</p>",
		},
	];
	return articles[id - 1];
}

//Le meto 500ms de retraso para que sea obvio que está cargando
function simularRetraso(callback) {
	return new Promise((resolve) => {
		setTimeout(() => {
			resolve(callback);
		}, 500);
	});
}
